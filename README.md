See the examples
================
The examples are rendered online on GitHub. ***You do not need to clone the project to see the examples.***

Edit the examples
==================
Clone the project. Start the containers by calling `docker-compose up -d`. 

The Jupyter Notebooks can be found at: `http://localhost:8888`

OpenRefine can be found at: `http://localhost:3333`

What's included
===============
Jupyter [Scipy-Notebook](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#jupyter-scipy-notebook) with Pandas etc.
